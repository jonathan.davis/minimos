# MinimOS
## Description
A small kernel written in C/C++ for fun/learning.

Some code borrowed from [osdev.org](www.osdev.org), as new features are tested/implemented for the first time.

## Instructions
1. Clone the repository and cd into it

   `git clone https://gitlab.com/jonathan.davis/minimos.git && cd minimos`
2. Configure `./compile` to contain the path to your cross-compiler
3. Run `./compile`
   This will generate build/ and sysroot/ and compile the kernel
4. Run `./iso`
   This will generate an .iso file
5. Start the kernel with `./qemu`

After starting the kernel, you should be presented with a screen saying "Hello, world!"

## Documentation

To be implemented.

