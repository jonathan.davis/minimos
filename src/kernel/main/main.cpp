/** @file main.cpp
 * File containing the kernel entry point.
 */

#include <kernel/kernel.h>
#include <machine/ram_bootstrap.h>
#include <machine/tty.h>


extern "C" {

/**
 * Kernel entry point.
 * Requires a pointer to the RAM bootstrap structure created by bootstrap_ram()
 * @param ram_info pointer to the RAM bootstrap structure
 */
void kernel_main(ram_bootstrap_t *ram_info)
{
    terminal_initialize();

    Kernel::init(ram_info);
    Kernel::start();
}

}

