#include <kernel/kernel.h>
#include <thread/sleep.h>
#include <thread/synchronization.h>

#include <stdio.h>

#define VERSION_MAJOR   0
#define VERSION_MINOR   1
#define VERSION_PATCH   0

Machine Kernel::m_machine;
Memory Kernel::m_memory;
Heap *Kernel::m_heap;
Scheduler Kernel::m_scheduler;

volatile bool mtx;


void task1()
{
    while (1) {
        {
            Spinlock lock(const_cast<bool *>(&mtx));
            printf("task 1\n");
        }
        sleep(1000);
    }
}

void task2()
{
    while (1) {
        {
            Spinlock lock(const_cast<bool *>(&mtx));
            printf("task 2\n");
        }
        sleep(1000);
    }
}


void Kernel::init(ram_bootstrap_t *ram_info)
{
    m_memory.init(ram_info);
    m_heap = m_memory.create_heap(KERNEL_HEAP_LOCATION, 0x4000, 0x10000000);
    m_machine.init();
    m_scheduler.init(ram_info);
}


void Kernel::start()
{
    printf("Hello, world!\n");

    m_scheduler.create_task(&task1);
    m_scheduler.create_task(&task2);

    while (1) {
        sleep(100);
    }
}
