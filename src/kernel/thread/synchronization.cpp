#include <machine/task.h>
#include <thread/synchronization.h>


Spinlock::Spinlock(bool *lock) :
    m_lock(lock)
{
    enter_region(m_lock);
}


Spinlock::~Spinlock()
{
    leave_region(m_lock);
}
