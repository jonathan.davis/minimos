#include <thread/sleep.h>
#include <machine/timer.h>


void sleep(uint32_t ticks)
{
    uint32_t time = timer_tick_count() + ticks;
    while (timer_tick_count() < time);
}
