#include <thread/task_queue.h>


void TaskQueue::enqueue(Task *task)
{
    if (!m_head) {
        m_head = task;
        m_tail = task;
    }
    else {
        m_tail->m_next = task;
        m_tail = task;
    }
}


Task *TaskQueue::dequeue()
{
    if (!m_head) {
        return nullptr;
    }

    Task *task = m_head;
    m_head = m_head->m_next;
    return task;
}
