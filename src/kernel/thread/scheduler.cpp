#include <kernel/kernel.h>
#include <machine/task.h>
#include <thread/scheduler.h>
#include <thread/synchronization.h>

#include <string.h>


void init_task()
{
    leave_region(Kernel::scheduler().get_lock());
}


void Scheduler::init(ram_bootstrap_t *ram_info)
{
    Task *kernel_task = static_cast<Task *>(Kernel::heap().alloc(sizeof(Task)));
    uint32_t stack = ram_info->m_stack_ptr + 0xBFF00000;
    kernel_task->m_id = 0;
    kernel_task->m_ebp = stack;
    kernel_task->m_esp = stack;
    kernel_task->m_page_directory = ram_info->m_page_dir;
    kernel_task->m_state = Task::State::running;
    kernel_task->m_next = nullptr;
    m_current_task = kernel_task;
}


void Scheduler::create_task(void (*eip)())
{
    void *stack = Kernel::memory().create_stack(0x4000);
    PageDirectory *p1 = Kernel::memory().clone_page_directory(
            reinterpret_cast<PageDirectory *>(m_current_task->m_page_directory));

    // frame 1: ebp, edi, esi, ebx, init_task eip
    // frame 2: desired eip
    uint32_t switch_stack[6] = {
            reinterpret_cast<uint32_t>(stack),
            0,
            0,
            0,
            reinterpret_cast<uint32_t>(&init_task),
            reinterpret_cast<uint32_t>(eip)
    };
    uint32_t stack_size = 6 * sizeof(uint32_t);

    Task *task = static_cast<Task *>(Kernel::heap().alloc(sizeof(Task)));
    task->m_id = m_next_id++;
    task->m_ebp = reinterpret_cast<uint32_t>(stack);
    task->m_esp = reinterpret_cast<uint32_t>(stack) - stack_size;
    task->m_page_directory = reinterpret_cast<uint32_t>(p1);
    task->m_state = Task::State::running;

    memcpy(reinterpret_cast<void *>(reinterpret_cast<uint32_t>(stack) - stack_size),
           reinterpret_cast<const void *>(switch_stack), stack_size);

    schedule(m_current_task);
    {
        Spinlock lock(&m_lock);
        ::switch_task(task, &m_current_task);
    }
}


void Scheduler::schedule(Task *task)
{
    task->m_state = Task::State::ready;
    m_ready_tasks.enqueue(task);
}


void Scheduler::block(Task::State state)
{
    Spinlock lock(&m_lock);
    m_current_task->m_state = state;
    switch_task();
}


void Scheduler::unblock(Task *task)
{
    Spinlock lock(&m_lock);
    schedule(task);
    switch_task();
}


void Scheduler::switch_task()
{
    Task *next_task = m_ready_tasks.dequeue();
    if (!next_task) {
        return;
    }

    if (m_current_task->m_state == Task::State::running) {
        m_current_task->m_state = Task::State::ready;
        m_ready_tasks.enqueue(m_current_task);
    }
    next_task->m_state = Task::State::running;
    ::switch_task(next_task, &m_current_task);
}


bool *Scheduler::get_lock()
{
    return &m_lock;
}
