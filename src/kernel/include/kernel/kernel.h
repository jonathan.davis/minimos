#ifndef _KERNEL_H
#define _KERNEL_H

#include <kernel/multiboot.h>
#include <machine/machine.h>
#include <machine/ram_bootstrap.h>
#include <memory/memory.h>
#include <memory/heap.h>
#include <thread/scheduler.h>


/**
 * The minimOS kernel.
 */
class Kernel
{
private:
    static Machine m_machine;  /**< Representation of the processor. */
    static Memory m_memory;    /**< Memory manager. @see Memory */
    static Heap *m_heap;
    static Scheduler m_scheduler;

public:
    /**
     * Initializes the minimOS kernel.
     * Performs all necessary startup routines for kernel initialization.
     * This includes processor specific tasks including global descriptor tables and interrupts as well as general
     * kernel tasks such as initializing the memory management system.
     * @param ram_info pointer to the RAM bootstrap structure provided by bootstrap_ram
     */
    static void init(ram_bootstrap_t *ram_info);

    /**
     * Starts the minimOS kernel.
     * @note This function should never return.
     */
    static void start();

    static Memory &memory() { return m_memory; }

    static Heap &heap() { return *m_heap; }

    static Scheduler &scheduler() { return m_scheduler; }
};

#endif

