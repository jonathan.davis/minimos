#ifndef _TIMER_H
#define _TIMER_H

#include <machine/isr.h>

#include <stdint.h>

void timer_interrupt(InterruptFrame *frame);

uint32_t timer_tick_count();


class Timer
{
protected:
    uint32_t m_freq;

public:
    virtual void set_frequency(uint32_t frequency) = 0;

    inline uint32_t frequency() const { return m_freq; }
};

#endif
