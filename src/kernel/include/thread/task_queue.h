#ifndef _TASK_QUEUE_H
#define _TASK_QUEUE_H

#include <machine/task.h>


class TaskQueue
{
private:
    Task *m_head;
    Task *m_tail;

public:
    void enqueue(Task *task);
    Task *dequeue();
};

#endif
