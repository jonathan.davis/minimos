#ifndef _SLEEP_H
#define _SLEEP_H

#include <stdint.h>


/**
 * Put the current thread to sleep.
 * Places the current thread to sleep for a specified number of clock ticks.
 * Ticks are registered by the timer interrupt.
 * @param ticks number of ticks to sleep for
 */
void sleep(uint32_t ticks);


#endif
