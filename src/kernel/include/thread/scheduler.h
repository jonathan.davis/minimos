#ifndef _SCHEDULER_H
#define _SCHEDULER_H

#include <machine/task.h>
#include <machine/ram_bootstrap.h>
#include <thread/task_queue.h>


class Scheduler
{
private:
    TaskQueue m_ready_tasks;
    Task *m_current_task;

    uint16_t m_next_id;
    bool m_lock;

public:
    void init(ram_bootstrap_t *ram_info);

    void create_task(void (*eip)());

    void schedule(Task *task);

    void block(Task::State state);

    void unblock(Task *task);

    void switch_task();

    bool *get_lock();
};

#endif
