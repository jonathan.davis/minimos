#ifndef _SYNCHRONIZATION_H
#define _SYNCHRONIZATION_H

#include <thread/task_queue.h>


struct Semaphore
{
    volatile unsigned int m_max_count;
    volatile unsigned int m_count;
    TaskQueue m_blocked_tasks;

    Semaphore(unsigned int max_count);
};


struct Mutex
{
    volatile bool m_acquired;
    volatile TaskQueue m_blocked_tasks;
};


class Spinlock
{
private:
    bool *m_lock;
public:
    Spinlock(bool *lock);
    ~Spinlock();
};

#endif
