#ifndef _ADDRESS_STACK_H
#define _ADDRESS_STACK_H

#include <stdint.h>


/**
 * Physical address stack.
 * Stores physical page addresses in two stacks, allowing lower and upper physical page allocations.
 */
class AddressStack
{
private:
    // TODO: Protect with mutex once multithreaded
    uint32_t *m_lower_stack;   /**< stack for addresses below 1 MiB. */
    uint32_t *m_upper_stack;   /**< stack for addresses above 1 MiB. */

    unsigned int m_lower_capacity;  /**< maximum number of lower page addresses. */
    unsigned int m_upper_capacity;  /**< maximum number of upper page addresses */
    unsigned int m_lower_size;      /**< number of free lower page addresses. */
    unsigned int m_upper_size;      /**< number of free upper page addresses. */
public:
    AddressStack() = default;

    /**
     * Address stack initialization.
     * Requires the locations in memory at which the stacks will reside, as well as their individual capacities.
     * @param lower_stack lower stack address
     * @param lower_capacity capacity of the lower address stack
     * @param upper_stack upper stack address
     * @param upper_capacity capacity of the upper address stack
     */
    void init(uint32_t *lower_stack, unsigned int lower_capacity, uint32_t *upper_stack, unsigned int upper_capacity);

    /**
     * Capacity of the lower address stack.
     * @return lower address stack capacity
     */
    unsigned int lower_capacity() const;

    /**
     * Capacity of the entire address stack.
     * This is the total of both the lower and upper stacks.
     * @return total address stack capacity
     */
    unsigned int capacity() const;

    /**
     * Number of free lower pages.
     * @return number of free lower pages
     */
    unsigned int lower_size() const;

    /**
     * Number of free pages.
     * This is the total number of free pages from both the lower and upper stacks.
     * @return total number of free pages
     */
    unsigned int size() const;

    /**
     * Free low page address from the top of the lower stack.
     * @return first free lower page address
     */
    uint32_t get_low_addr() const;

    /**
     * Free page address.
     * First returns a free address from the upper stack. If the upper stack is empty, it will then return an address
     * from the lower stack.
     * @return free page address
     */
    uint32_t get_addr() const;

    /**
     * Pops an address off of the lower stack.
     */
    void pop_low_addr();

    /**
     * Pops the first free address off of the stack.
     * First pops an address from the upper stack. If the upper stack is empty, it will pop an address from the lower
     * stack.
     */
    void pop_addr();

    /**
     * Pushes an address onto the stack.
     * Automatically pushes an address onto the lower or upper stack depending on whether it is below or above 1 MiB.
     * @param addr address to push
     * @return returns true if the operation was successful
     */
    bool push_addr(uint32_t addr);
};


#endif
