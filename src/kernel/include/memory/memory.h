#ifndef _MEMORY_H
#define _MEMORY_H

#include <kernel/multiboot.h>
#include <machine/paging.h>
#include <machine/ram_bootstrap.h>
#include <memory/address_mapper.h>
#include <memory/heap.h>
#include <memory/physical_page_allocator.h>
#include <memory/virtual_page_allocator.h>

#define KERNEL_HEAP_LOCATION    0xD0000000


/**
 * Memory manager.
 * Allocates and deallocates physical pages and manages virtual mappings to them.
 */
class Memory
{
private:
    AddressMapper m_mapper;
    PhysicalPageAllocator m_physical_allocator;    /**< Physical page allocator. @see PhysicalPageAllocator */
    VirtualPageAllocator m_virtual_allocator;      /**< List of free virtual addresses. @see VirtualPageAllocator */
    PageDirectory *m_kernel_directory;

    Page clone_page(Page &page);
    PageTable *clone_page_table(PageTable *table);
public:
    Memory() = default;

    /**
     * Initializes the memory manager.
     * Requires a pointer to the RAM bootstrap structure.
     * @param ram_info pointer to the RAM bootstrap structure
     */
    void init(ram_bootstrap_t *ram_info);

    Heap *create_heap(uint32_t base, uint32_t size, uint32_t max_size);

    void free_heap(Heap *heap);

    void *create_stack(uint32_t size);

    PageDirectory *clone_page_directory(PageDirectory *dir);

    void free_page_directory(PageDirectory *dir);
};


#endif
