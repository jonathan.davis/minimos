#ifndef _HEAP_H
#define _HEAP_H

#include <stdint.h>


class Heap
{
private:
    struct Metadata {
        uint32_t m_size;
    };

    uint32_t m_base;
    uint32_t m_size;
    uint32_t m_max_size;
    uint32_t m_placement_addr;

    void place_metadata(uint32_t size);
public:
    Heap(uint32_t base, uint32_t size, uint32_t max_size);

    void *alloc(uint32_t size);
    void free(void *addr);

    uint32_t base();
};

#endif
