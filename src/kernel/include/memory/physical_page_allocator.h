#ifndef _PHYSICAL_PAGE_ALLOCATOR_H
#define _PHYSICAL_PAGE_ALLOCATOR_H

#include <kernel/multiboot.h>
#include <memory/address_stack.h>


/**
 * Physical page allocator.
 * Stores free physical pages in an Address Stack.
 * @see AddressStack
 */
class PhysicalPageAllocator
{
private:
    AddressStack m_address_stack;   /**< Address stack containing free pages. @see AddressStack */
public:
    PhysicalPageAllocator() = default;

    /**
     * Initializes the page allocator.
     * Requires a region to allocate the AddressStack.
     * @see AddressStack
     * @param lower_stack address of the lower stack
     * @param lower_capacity capacity of the lower stack
     * @param upper_stack address of the upper stack
     * @param upper_capacity capacity of the upper stack
     */
    void init(uint32_t *lower_stack, unsigned int lower_capacity, uint32_t *upper_stack, unsigned int upper_capacity);

    /**
     * Allocates a physical page.
     * Pops a single page address from the stack and returns it.
     * @return physical page address
     */
    uint32_t alloc_page();

    /**
     * Deallocates a physical page.
     * Pushes a single page address onto the stack.
     * @param page page to deallocate
     */
    void dealloc_page(uint32_t page);

    /**
     * Returns number of free physical pages.
     * @return number of free physical pages.
     */
    unsigned int size() const;
};


#endif
