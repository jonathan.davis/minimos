#ifndef _ADDRESS_MAPPER_H
#define _ADDRESS_MAPPER_H

#include <machine/paging.h>
#include <memory/physical_page_allocator.h>


class AddressMapper
{
private:
    PhysicalPageAllocator *m_allocator;
    PageDirectory *m_current_directory;

    PageTable *get_page_table(uint32_t virt_addr);
    PageTable *get_or_create_page_table(uint32_t virt_addr);
    Page *get_page(uint32_t virt_addr);
    Page *get_or_create_page(uint32_t virt_addr);
public:
    void init(PhysicalPageAllocator *allocator);

    PageDirectory *set_directory(PageDirectory *dir);

    /**
     * Returns whether a virtual address is present in the page directory.
     * @param virt_addr virtual address to check
     * @return true if the address is present, false otherwise
     */
    bool is_present(uint32_t virt_addr);

    /**
     * Returns the physical address mapped to the given virtual address.
     * @param virt_addr virtual address to convert
     * @return mapped physical address or nullptr if not present
     */
    uint32_t get_physical_addr(uint32_t virt_addr);

    /**
     * Maps the physical address to the virtual address with the given flags.
     * @param phys_addr physical address to map
     * @param virt_addr virtual address to map
     * @param flags directory entry flags
     */
    void map_addr(uint32_t phys_addr, uint32_t virt_addr, uint16_t flags);

    /**
     * Unmaps the virtual address.
     * Removes the mapping in the page directory and returns the physical address.
     * @param virt_addr virtual address to unmap
     * @return physical address that was unmapped
     */
    uint32_t unmap_addr(uint32_t virt_addr);

    // TODO: determine if necessary and required operation
    void flush();

    void load();
};

#endif
