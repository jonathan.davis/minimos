#ifndef _VIRTUAL_PAGE_ALLOCATOR__H
#define _VIRTUAL_PAGE_ALLOCATOR__H

#include <stdint.h>


/**
 * Virtual address allocator.
 * Stores free contiguous virtual address ranges in sorted order.
 * Allocates by first fit policy.
 */
class VirtualPageAllocator
{
private:
    /**
     * List node.
     * Contains the base address of the range, the size, and pointers to the preceding and succeeding nodes.
     */
    struct Node {
        uint32_t m_addr;    /**< The base address of the range. */
        uint32_t m_size;    /**< The size of the range in bytes. */
        Node *m_prev;       /**< Pointer to the previous Node. */
        Node *m_next;       /**< Pointer to the next Node. */
    };

    Node *m_base;   /**< Base address of usable memory. This is the region in which Nodes may be allocated. */
    Node *m_limit;  /**< Limit address of usable memory */

    Node *m_head;               /**< Pointer to the head of the list. */
    unsigned int m_size;        /**< Number of nodes in the list. */
    unsigned int m_capacity;    /**< Maximum number of nodes in the list. */

    /**
     * Allocates a new Node.
     * Searches the address space starting from m_base for a Node with size 0.
     * If a Node with size 0 is found, its address is returned. Otherwise nullptr is returned.
     * @see Node
     * @see m_base
     * @return pointer to the free Node; nullptr on failure
     */
    Node *allocate_node();

    /**
     * Frees a node.
     * Sets all of the fields in the Node to 0, marking it as unused.
     * @see Node
     * @param node pointer to the Node to free
     */
    void free_node(Node *node);

    /**
     * Inserts a new Node before an existing Node.
     * If the existing Node is the head of the list, then the new Node becomes the head.
     * @see Node
     * @param new_node pointer to the new Node
     * @param node pointer to the existing Node
     */
    void insert_before(Node *new_node, Node *node);

    /**
     * Inserts a new Node after an existing Node.
     * @see Node
     * @param new_node pointer to the new Node
     * @param node pointer to the existing Node
     */
    void insert_after(Node *new_node, Node *node);

    /**
     * Removes an existing Node.
     * Unlinks the Node with its neighbors and links the neighbors together.
     * If the Node is the head of the list, then its succeeding Node becomes the head.
     * Frees the Node upon finishing the linking process.
     * @see Node
     * @see free_node()
     * @param node pointer to the Node to remove
     */
    void remove(Node *node);

    /**
     * Attempts to merge a Node with its neighbors.
     * If the given Node is found to be contiguous with either of its neighbors, they will be consolidated.
     * Consolidated nodes are then removed.
     * @see Node
     * @see remove()
     * @param node pointer to the Node to merge
     */
    void try_merge(Node *node);

    Node *split_node(Node *node, uint32_t addr);
public:
    VirtualPageAllocator() = default;

    /**
     * Address list initialization.
     * Requires the location in memory at which Nodes will be stored, as well as the size of the region.
     * @see Node
     * @see allocate_node()
     * @param base pointer to free memory
     * @param initial_size initial size of the free memory provided
     */
    void init(uint32_t *base, uint32_t initial_size);

    /**
     * Free contiguous virtual addresses.
     * @param addr base address of the range
     * @param size size of the range in bytes
     */
    void free(uint32_t addr, uint32_t size);

    /**
     * Allocate contiguous virtual addresses.
     * Searches the list for the first fit and returns the address.
     * @param size size of range to allocate in bytes
     * @return address of range
     */
    uint32_t alloc(uint32_t size);

    uint32_t alloc(uint32_t address, uint32_t size);

    /**
     * Returns the number of contiguous regions of available virtual memory
     * @return number of contiguous regions of available virtual memory
     */
    unsigned int size() const;
};


#endif
