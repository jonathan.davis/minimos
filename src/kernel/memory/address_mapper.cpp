#include <kernel/kernel.h>
#include <memory/address_mapper.h>

#include <string.h>


PageTable *AddressMapper::get_page_table(uint32_t virt_addr)
{
    unsigned int pde_idx = virt_addr >> 22;
    PageTable *table = reinterpret_cast<PageTable **>(PAGE_DIRECTORY_VIRTUAL_ADDRESS)[pde_idx];
    return table;
}


PageTable *AddressMapper::get_or_create_page_table(uint32_t virt_addr)
{
    unsigned int pde_idx = virt_addr >> 22;
    PageTable *table = reinterpret_cast<PageTable **>(PAGE_DIRECTORY_VIRTUAL_ADDRESS)[pde_idx];

    if (reinterpret_cast<uint32_t>(table) & 1) {
        return table;
    }

    uint32_t paddr = m_allocator->alloc_page();
    table = reinterpret_cast<PageTable *>(paddr | 0x3);
    reinterpret_cast<PageTable **>(PAGE_DIRECTORY_VIRTUAL_ADDRESS)[pde_idx] = table;
    memset(reinterpret_cast<void *>(PAGE_TABLE0_VIRTUAL_ADDRESS + pde_idx * PAGE_SIZE), 0, PAGE_SIZE);

    return table;
}


Page *AddressMapper::get_page(uint32_t virt_addr)
{
    if (!(reinterpret_cast<uint32_t>(get_page_table(virt_addr)) & 0x1)) {
        return nullptr;
    }

    unsigned int pde_idx = virt_addr >> 22;
    unsigned int pte_idx = (virt_addr >> 12) & 0x3FF;
    PageTable *table = reinterpret_cast<PageTable *>(PAGE_TABLE0_VIRTUAL_ADDRESS + pde_idx * PAGE_SIZE);
    Page *page = &table->m_entries[pte_idx];
    return page;
}

Page *AddressMapper::get_or_create_page(uint32_t virt_addr)
{
    get_or_create_page_table(virt_addr);

    unsigned int pde_idx = virt_addr >> 22;
    unsigned int pte_idx = (virt_addr >> 12) & 0x3FF;
    PageTable *table = reinterpret_cast<PageTable *>(PAGE_TABLE0_VIRTUAL_ADDRESS + pde_idx * PAGE_SIZE);
    Page *page = &table->m_entries[pte_idx];
    return page;
}


void AddressMapper::init(PhysicalPageAllocator *allocator)
{
    m_allocator = allocator;
}


PageDirectory *AddressMapper::set_directory(PageDirectory *dir)
{
    PageDirectory *old = m_current_directory;
    m_current_directory = dir;
    return old;
}


void AddressMapper::map_addr(uint32_t phys_addr, uint32_t virt_addr, uint16_t flags)
{
    Page *page = get_or_create_page(virt_addr);
    if (!page) {
        return;
    }
    page->m_address = phys_addr >> 12;
    page->m_flags = flags;
}


uint32_t AddressMapper::unmap_addr(uint32_t virt_addr)
{
    Page *page = get_page(virt_addr);
    if (!page) {
        return 0x0;
    }
    uint32_t phys_addr = page->m_address << 12;
    page->m_flags &= ~(0x1);

    return phys_addr;
}


uint32_t AddressMapper::get_physical_addr(uint32_t virt_addr)
{
    Page *page = get_page(virt_addr);
    return page->m_address << 12;
}


bool AddressMapper::is_present(uint32_t virt_addr)
{
    Page *page = get_page(virt_addr);
    if (!page) {
        return false;
    }
    return page->m_flags & 0x1;
}


void AddressMapper::flush()
{
    asm volatile("movl %cr3, %ecx");
    asm volatile("movl %ecx, %cr3");
}


void AddressMapper::load()
{
    asm volatile("movl %0, %%cr3" : : "r" (m_current_directory));
}
