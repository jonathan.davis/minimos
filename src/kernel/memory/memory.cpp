#include <memory/memory.h>

#include <string.h>
#include "../include/memory/memory.h"

extern uint32_t _kernel_vma;
extern uint32_t _kernel_end;
extern uint32_t _kernel_offset;


Page Memory::clone_page(Page &page)
{
    Page new_page = {page.m_flags, m_physical_allocator.alloc_page() >> 12};    // dup flags, get new frame

    void *tmp_page = reinterpret_cast<void *>(m_virtual_allocator.alloc(PAGE_SIZE));
    void *tmp_new = reinterpret_cast<void *>(m_virtual_allocator.alloc(PAGE_SIZE));
    m_mapper.map_addr(page.m_address << 12, reinterpret_cast<uint32_t>(tmp_page), 0x3);
    m_mapper.map_addr(new_page.m_address << 12, reinterpret_cast<uint32_t>(tmp_new), 0x3);
    m_mapper.flush();
    memcpy(tmp_new, tmp_page, PAGE_SIZE);
    m_mapper.unmap_addr(reinterpret_cast<uint32_t>(tmp_page));
    m_mapper.unmap_addr(reinterpret_cast<uint32_t>(tmp_new));
    m_mapper.flush();
    m_virtual_allocator.free(reinterpret_cast<uint32_t>(tmp_page), PAGE_SIZE);
    m_virtual_allocator.free(reinterpret_cast<uint32_t>(tmp_new), PAGE_SIZE);

    return new_page;
}


PageTable *Memory::clone_page_table(PageTable *table)
{
    PageTable *new_table = reinterpret_cast<PageTable *>(m_physical_allocator.alloc_page());

    // Temporarily map to an address to clone
    PageTable *tmp_table = reinterpret_cast<PageTable *>(m_virtual_allocator.alloc(PAGE_SIZE));
    PageTable *tmp_new = reinterpret_cast<PageTable *>(m_virtual_allocator.alloc(PAGE_SIZE));
    m_mapper.map_addr(reinterpret_cast<uint32_t>(table), reinterpret_cast<uint32_t>(tmp_table), 0x3);
    m_mapper.map_addr(reinterpret_cast<uint32_t>(new_table), reinterpret_cast<uint32_t>(tmp_new), 0x3);
    m_mapper.flush();
    for (int i = 0; i < PAGE_DIRECTORY_SIZE; ++i) {
        Page page = tmp_table->m_entries[i];
        if (page.m_address != 0x0) {
            tmp_new->m_entries[i] = clone_page(page);
        }
        else {
            tmp_new->m_entries[i] = {0x0, 0x0};
        }
    }
    m_mapper.unmap_addr(reinterpret_cast<uint32_t>(tmp_table));
    m_mapper.unmap_addr(reinterpret_cast<uint32_t>(tmp_new));
    m_mapper.flush();
    m_virtual_allocator.free(reinterpret_cast<uint32_t>(tmp_table), PAGE_SIZE);
    m_virtual_allocator.free(reinterpret_cast<uint32_t>(tmp_new), PAGE_SIZE);


    return new_table;
}


void Memory::init(ram_bootstrap_t *ram_info)
{
    // Save kernel page directory
    m_kernel_directory = reinterpret_cast<PageDirectory *>(ram_info->m_page_dir);

    // Initialize allocators
    m_physical_allocator.init(
            reinterpret_cast<uint32_t *>(ram_info->m_physical_stack_lower + reinterpret_cast<uint32_t>(&_kernel_offset)),
            ram_info->m_physical_stack_lower_capacity,
            reinterpret_cast<uint32_t *>(ram_info->m_physical_stack_upper + reinterpret_cast<uint32_t>(&_kernel_offset)),
            ram_info->m_physical_stack_upper_capacity);

    m_virtual_allocator.init(
            reinterpret_cast<uint32_t *>(ram_info->m_virtual_list + reinterpret_cast<uint32_t>(&_kernel_offset)),
            ram_info->m_virtual_list_limit);

    m_mapper.init(&m_physical_allocator);
    m_mapper.set_directory(reinterpret_cast<PageDirectory *>(ram_info->m_page_dir));


    // Push free physical pages onto the address stack
    multiboot_info_t *mbd = ram_info->m_multiboot_info;
    uint32_t entry_addr = mbd->mmap_addr;
    while (entry_addr < mbd->mmap_addr + mbd->mmap_length) {
        multiboot_memory_map_t *entry = reinterpret_cast<multiboot_memory_map_t *>(entry_addr);

        if (entry->type == MULTIBOOT_MEMORY_AVAILABLE) {
            uint32_t addr = entry->addr_low;
            while (addr + 0x1000 <= entry->addr_low + entry->len_low) {
                // Only push pages outside the kernel
                uint32_t kernel_low = &_kernel_vma - &_kernel_offset;
                uint32_t kernel_high = &_kernel_end - &_kernel_offset;
                if (addr < kernel_low || addr > kernel_high) {
                    m_physical_allocator.dealloc_page(addr);
                }

                addr += 0x1000;
            }
        }

        entry_addr += entry->size + sizeof(entry->size);
    }


    // Add free addresses to the list
    uint32_t addr = 0x00100000;
    uint32_t size = reinterpret_cast<uint32_t>(&_kernel_vma) - addr;
    m_virtual_allocator.free(addr, size);

    // TODO: Better way of determining start of free mem
    addr = ram_info->m_virtual_list + ram_info->m_virtual_list_limit;
    addr += reinterpret_cast<uint32_t>(&_kernel_offset);
    size = PAGE_TABLE0_VIRTUAL_ADDRESS - addr;
    m_virtual_allocator.free(addr, size);

    // Remove identity mapped kernel code
    for (uint32_t addr = 0x00100000; addr < 0x00200000; addr += 0x1000) {
        m_mapper.unmap_addr(addr);
    }

    // Remove unused mappings starting from end of virtual memory allocator
    // TODO: Better way of determining start of free mem
    addr = ram_info->m_virtual_list + ram_info->m_virtual_list_limit;
    addr += reinterpret_cast<uint32_t>(&_kernel_offset);
    while (addr < 0xFFC00000 && m_mapper.is_present(addr)) {
        m_mapper.unmap_addr(addr);
        addr += 0x1000;
    }
    m_mapper.flush();
}


Heap *Memory::create_heap(uint32_t base, uint32_t size, uint32_t max_size)
{
    // TODO: page align
    uint32_t vaddr = m_virtual_allocator.alloc(base, size);
    if (!vaddr) {
        return nullptr;
    }

    for (uint32_t page = 0; page < size; page += 0x1000) {
        uint32_t paddr = m_physical_allocator.alloc_page();
        m_mapper.map_addr(paddr, vaddr + page, 0x3);
    }

    Heap *heap = reinterpret_cast<Heap *>(base);
    *heap = Heap(base, size, max_size);
    return heap;
}


void Memory::free_heap(Heap *heap)
{

}


void *Memory::create_stack(uint32_t size)
{
    uint32_t vaddr = m_virtual_allocator.alloc(size);
    if (!vaddr) {
        return nullptr;
    }

    for (uint32_t page = 0; page < size; page += 0x1000) {
        uint32_t paddr = m_physical_allocator.alloc_page();
        m_mapper.map_addr(paddr, vaddr + page, 0x3);
    }

    return reinterpret_cast<void *>(vaddr + size);
}


PageDirectory *Memory::clone_page_directory(PageDirectory *dir)
{
    PageDirectory *new_dir = reinterpret_cast<PageDirectory *>(m_physical_allocator.alloc_page());

    // Temporarily map to an address to clone
    PageDirectory *tmp_dir = reinterpret_cast<PageDirectory *>(m_virtual_allocator.alloc(PAGE_SIZE));
    PageDirectory *tmp_new = reinterpret_cast<PageDirectory *>(m_virtual_allocator.alloc(PAGE_SIZE));
    m_mapper.map_addr(reinterpret_cast<uint32_t>(dir), reinterpret_cast<uint32_t>(tmp_dir), 0x3);
    m_mapper.map_addr(reinterpret_cast<uint32_t>(new_dir), reinterpret_cast<uint32_t>(tmp_new), 0x3);
    m_mapper.flush();
    for (int i = 0; i < PAGE_DIRECTORY_SIZE-1; ++i) {
        PageTable *table = tmp_dir->m_entries[i];
        if (table) {
            if (table == reinterpret_cast<PageDirectory *>(PAGE_DIRECTORY_VIRTUAL_ADDRESS)->m_entries[i]) {
                tmp_new->m_entries[i] = table;  // Link pages in the kernel directory
            }
            else {
                uint32_t entry = reinterpret_cast<uint32_t>(clone_page_table(table)) | 0x3;
                tmp_new->m_entries[i] = reinterpret_cast<PageTable *>(entry);
            }
        }
        else {
            tmp_new->m_entries[i] = nullptr;
        }
    }
    uint32_t entry = reinterpret_cast<uint32_t>(new_dir) | 0x3;
    tmp_new->m_entries[PAGE_TABLE_SIZE-1] = reinterpret_cast<PageTable *>(entry);   // Always map dir to itself
    m_mapper.unmap_addr(reinterpret_cast<uint32_t>(tmp_dir));
    m_mapper.unmap_addr(reinterpret_cast<uint32_t>(tmp_new));
    m_mapper.flush();
    m_virtual_allocator.free(reinterpret_cast<uint32_t>(tmp_dir), PAGE_SIZE);
    m_virtual_allocator.free(reinterpret_cast<uint32_t>(tmp_new), PAGE_SIZE);

    return new_dir;
}


void Memory::free_page_directory(PageDirectory *dir)
{

}
