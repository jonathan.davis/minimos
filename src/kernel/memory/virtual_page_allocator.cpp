#include <memory/virtual_page_allocator.h>


VirtualPageAllocator::Node *VirtualPageAllocator::allocate_node()
{
    VirtualPageAllocator::Node *node = m_base;
    for (int i = 0; i < m_capacity; ++i) {
        if (node->m_size == 0) {
            return node;
        }
        node++;
    }
    return nullptr;
}


void VirtualPageAllocator::free_node(VirtualPageAllocator::Node *node)
{
    node->m_addr = 0x0;
    node->m_size = 0;
    node->m_next = nullptr;
    node->m_prev = nullptr;
}


void VirtualPageAllocator::insert_before(VirtualPageAllocator::Node *new_node, VirtualPageAllocator::Node *node)
{
    new_node->m_next = node;
    new_node->m_prev = node->m_prev;
    if (new_node->m_prev) {
        new_node->m_prev->m_next = new_node;
    }
    node->m_prev = new_node;
    if (node == m_head) {
        m_head = new_node;
    }
    m_size++;
}



void VirtualPageAllocator::insert_after(VirtualPageAllocator::Node *new_node, VirtualPageAllocator::Node *node)
{
    new_node->m_prev = node;
    new_node->m_next = node->m_next;
    if (new_node->m_next) {
        new_node->m_next->m_prev = new_node;
    }
    node->m_next = new_node;
    m_size++;
}


void VirtualPageAllocator::remove(VirtualPageAllocator::Node *node)
{
    if (node == m_head) {
        m_head = node->m_next;
        m_head->m_prev = nullptr;
    }
    else {
        node->m_prev->m_next = node->m_next;
        if (node->m_next) {
            node->m_next->m_prev = node->m_prev;
        }
    }

    free_node(node);
    m_size--;
}


void VirtualPageAllocator::try_merge(VirtualPageAllocator::Node *node)
{
    VirtualPageAllocator::Node *prev = node->m_prev;
    VirtualPageAllocator::Node *next = node->m_next;

    if (next && reinterpret_cast<uint32_t>(node->m_addr) + node->m_size == reinterpret_cast<uint32_t>(next->m_addr)) {
        node->m_size += next->m_size;
        remove(next);
    }
    if (prev && reinterpret_cast<uint32_t>(prev->m_addr) + prev->m_size == reinterpret_cast<uint32_t>(node->m_addr)) {
        prev->m_size += node->m_size;
        remove(node);
    }
}


VirtualPageAllocator::Node *VirtualPageAllocator::split_node(VirtualPageAllocator::Node *node, uint32_t addr)
{
    if (node->m_addr == addr) {
        return node;
    }
    else if (addr < node->m_addr || addr > node->m_addr + node->m_size) {
        return nullptr;
    }

    VirtualPageAllocator::Node *new_node = allocate_node();
    if (!new_node) {
        return nullptr;
    }

    new_node->m_addr = addr;
    new_node->m_size = node->m_size - (addr - node->m_addr);
    node->m_size = addr - node->m_addr;
    insert_after(new_node, node);

    return new_node;
}


void VirtualPageAllocator::init(uint32_t *base, uint32_t initial_size)
{
    m_size = 0;
    m_capacity = initial_size / sizeof(VirtualPageAllocator::Node);
    m_base = reinterpret_cast<VirtualPageAllocator::Node *>(base);
    m_limit = m_base + m_capacity;

    m_head = m_base;
    free_node(m_head);
}


void VirtualPageAllocator::free(uint32_t addr, uint32_t size)
{
    // TODO: ensure page alignment
    if (m_size == 0) {
        m_head = m_base;
        m_head->m_addr = addr;
        m_head->m_size = size;
        m_head->m_prev = nullptr;
        m_head->m_next = nullptr;
        m_size = 1;
        return;
    }

    VirtualPageAllocator::Node *new_node = allocate_node();
    new_node->m_addr = addr;
    new_node->m_size = size;

    if (addr < m_head->m_addr) {
        insert_before(new_node, m_head);
        try_merge(new_node);
        return;
    }

    VirtualPageAllocator::Node *cur = m_head;
    while (cur->m_next && cur->m_next->m_addr < addr) {
        cur = cur->m_next;
    }
    insert_after(new_node, cur);
    try_merge(new_node);
}


uint32_t VirtualPageAllocator::alloc(uint32_t size)
{
    VirtualPageAllocator::Node *cur = m_head;
    while (cur && cur->m_size < size) {
        cur = cur->m_next;
    }

    if (!cur) {
        return 0x0;
    }

    uint32_t addr = cur->m_addr;
    cur->m_addr += size;
    cur->m_size -= size;

    if (cur->m_size == 0) {
        remove(cur);
    }

    return addr;
}


uint32_t VirtualPageAllocator::alloc(uint32_t addr, uint32_t size)
{
    VirtualPageAllocator::Node *cur = m_head;
    while (cur && cur->m_addr + cur->m_size < addr) {
        cur = cur->m_next;
    }

    if (!cur) {
        return 0x0;
    }
    else if (cur->m_size - addr < size) {
        return 0x0;
    }

    VirtualPageAllocator::Node *split = split_node(cur, addr);
    split->m_addr += size;
    split->m_size -= size;

    if (split->m_size == 0) {
        remove(split);
    }

    return addr;
}


unsigned int VirtualPageAllocator::size() const
{
    return m_size;
}
