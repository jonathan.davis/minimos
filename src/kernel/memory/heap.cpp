#include <memory/heap.h>
#include <kernel/kernel.h>


Heap::Heap(uint32_t base, uint32_t size, uint32_t max_size) :
    m_base(base),
    m_size(size),
    m_max_size(max_size),
    m_placement_addr(base + sizeof(Heap))
{

}


void Heap::place_metadata(uint32_t size)
{
    Metadata *metadata = reinterpret_cast<Metadata *>(m_placement_addr);
    metadata->m_size = size;
    m_placement_addr += sizeof(Metadata);
}


void *Heap::alloc(uint32_t size)
{
    uint32_t total_size = sizeof(Metadata) + size;
    if (m_base + m_size - m_placement_addr < total_size) {
        return nullptr;
    }

    place_metadata(size);
    uint32_t addr = m_placement_addr;
    m_placement_addr += size;
    return reinterpret_cast<void *>(addr);
}


void Heap::free(void *addr)
{

}


uint32_t Heap::base()
{
    return m_base;
}
