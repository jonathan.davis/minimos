#include <memory/physical_page_allocator.h>


void PhysicalPageAllocator::init(uint32_t *lower_stack, unsigned int lower_capacity, uint32_t *upper_stack, unsigned int upper_capacity)
{
    m_address_stack.init(lower_stack, lower_capacity, upper_stack, upper_capacity);
}


uint32_t PhysicalPageAllocator::alloc_page()
{
    uint32_t page = m_address_stack.get_addr();
    m_address_stack.pop_addr();
    return page;
}


void PhysicalPageAllocator::dealloc_page(uint32_t page)
{
    m_address_stack.push_addr(page);
}


unsigned int PhysicalPageAllocator::size() const
{
    return m_address_stack.size();
}