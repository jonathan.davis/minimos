#include <memory/address_stack.h>


void AddressStack::init(uint32_t *lower_stack, unsigned int lower_capacity, uint32_t *upper_stack, unsigned int upper_capacity)
{
    m_lower_stack = lower_stack;
    m_lower_capacity = lower_capacity;
    m_upper_stack = upper_stack;
    m_upper_capacity = upper_capacity;
}


unsigned int AddressStack::lower_capacity() const
{
    return m_lower_capacity;
}


unsigned int AddressStack::capacity() const
{
    return m_lower_capacity + m_upper_capacity;
}


unsigned int AddressStack::lower_size() const
{
    return m_lower_size;
}


unsigned int AddressStack::size() const
{
    return m_lower_size + m_upper_size;
}


uint32_t AddressStack::get_low_addr() const
{
    // TODO: assertion
    return m_lower_stack[m_lower_size - 1];
}


uint32_t AddressStack::get_addr() const
{
    // TODO: assertion
    if (m_upper_size > 0) {
        return m_upper_stack[m_upper_size - 1];
    }
    return get_low_addr();
}


void AddressStack::pop_low_addr()
{
    // TODO: assertion
    m_lower_size--;
}


void AddressStack::pop_addr()
{
    // TODO: assertion
    if (m_upper_size > 0) {
        m_upper_size--;
    }
    else {
        pop_low_addr();
    }
}


bool AddressStack::push_addr(uint32_t addr)
{
    // TODO: assertion
    if (addr < 0x100000) {
        m_lower_stack[m_lower_size++] = addr;
    }
    else {
        m_upper_stack[m_upper_size++] = addr;
    }

    return true;

}
