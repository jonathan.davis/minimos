#include <stdio.h>

#include <kernel/kernel.h>
#include <machine/isr.h>
#include <machine/common.h>
#include <machine/pic.h>
#include <thread/synchronization.h>

__attribute__((interrupt))
void divide_by_zero_handler(InterruptFrame *frame)
{
    printf("Divide by zero error\n");
}

__attribute__((interrupt))
void debug_handler(InterruptFrame *frame)
{
    printf("Debug interrupt\n");
}

__attribute__((interrupt))
void nonmaskable_interrupt_handler(InterruptFrame *frame)
{
    printf("Non-maskable interrupt\n");
}

__attribute__((interrupt))
void breakpoint_handler(InterruptFrame *frame)
{
    printf("Breakpoint interrupt\n");
}

__attribute__((interrupt))
void overflow_handler(InterruptFrame *frame)
{
    printf("Overflow interrupt\n");
}

__attribute__((interrupt))
void bound_range_exceeded_handler(InterruptFrame *frame)
{
    printf("Bound range exceeded error\n");
}

__attribute__((interrupt))
void invalid_opcode_handler(InterruptFrame *frame)
{
    printf("Invalid opcode error\n");
}

__attribute__((interrupt))
void device_not_available_handler(InterruptFrame *frame)
{
    printf("Device not available error\n");
}

__attribute__((interrupt))
void double_fault_handler(InterruptFrame *frame, uint32_t err_code)
{
    printf("Double fault\n");
}

__attribute__((interrupt))
void coprocessor_segment_overrun_handler(InterruptFrame *frame)
{
    printf("Coprocessor segment overrun error\n");
}

__attribute__((interrupt))
void invalid_tss_handler(InterruptFrame *frame, uint32_t err_code)
{
    printf("Invalid TSS exception\n");
}

__attribute__((interrupt))
void segment_not_present_handler(InterruptFrame *frame, uint32_t err_code)
{
    printf("Segment not present exception\n");
}

__attribute__((interrupt))
void stack_segment_fault_handler(InterruptFrame *frame, uint32_t err_code)
{
    printf("Stack segment fault\n");
}

__attribute__((interrupt))
void general_protection_handler(InterruptFrame *frame, uint32_t err_code)
{
    printf("General protection fault\n");
}

__attribute__((interrupt))
void page_fault_handler(InterruptFrame *frame, uint32_t err_code)
{
    uint32_t addr;
    asm volatile("movl %%cr2, %0" : "=r" (addr));
    printf("Page fault:\n");
    printf("\taccessed: %x\n", addr);
    printf("\terr code: %x\n", err_code);
    printf("\tfault ip: %x\n", frame->eip);
    printf("\teflags:   %x\n", frame->eflags);
}

__attribute__((interrupt))
void math_fault_handler(InterruptFrame *frame)
{
    printf("Math fault\n");
}

__attribute__((interrupt))
void alignment_check_handler(InterruptFrame *frame, uint32_t err_code)
{
    printf("Alignment check exception\n");
}

__attribute__((interrupt))
void machine_check_handler(InterruptFrame *frame)
{
    printf("Machine check interrupt\n");
}

__attribute__((interrupt))
void simd_fpe_handler(InterruptFrame *frame)
{
    printf("SIMD floating-point error\n");
}

__attribute__((interrupt))
void virtualization_handler(InterruptFrame *frame)
{
    printf("Virtualization error\n");
}


uint32_t tick_count = 0;

__attribute__((interrupt))
void timer_interrupt(InterruptFrame *frame)
{
    PIC::send_eoi(static_cast<uint8_t>(Interrupt::timer));  // TODO: shouldn't be at beginning, but here it is for multitasking
    tick_count++;
    if (tick_count % 100 == 0) {
        Spinlock lock(Kernel::scheduler().get_lock());
        Kernel::scheduler().switch_task();
    }
}

uint32_t timer_tick_count()
{
    return tick_count;
}


__attribute__((interrupt))
void keyboard_interrupt(InterruptFrame *frame)
{
    printf("Keyboard interrupt\n");
    int a = inb(0x60);
    PIC::send_eoi(static_cast<uint8_t>(Interrupt::keyboard));
}
