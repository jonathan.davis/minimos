#include <machine/paging.h>

#include <string.h>

extern uint32_t _kernel_lma;
extern uint32_t _kernel_vma;
extern uint32_t _kernel_offset;
extern uint32_t _text_start;
extern uint32_t _text_end;
extern uint32_t _rodata_start;
extern uint32_t _rodata_end;


extern "C" {

void enable_paging(uint32_t **dir, uint32_t *identity_table, uint32_t *offset_table)
{
    // Clear page directory
    memset(dir, 0, PAGE_SIZE);

    // Identity map lowest 2 MiB
    uint32_t paddr = 0x00000000;    // physical addr to map
    for (int i = 0; i < 512; ++i) {
        identity_table[i] = paddr | 0x3;     // mark present, read-write
        paddr += 0x1000;
    }
    dir[0] = (uint32_t *)((uint32_t)identity_table | 0x3);

    // Map first 4 MiB of the kernel to kernel offset
    paddr = (uint32_t)&_kernel_lma;
    uint32_t text_start = (uint32_t)&_text_start - (uint32_t)&_kernel_offset;
    uint32_t text_end = (uint32_t)&_text_end - (uint32_t)&_kernel_offset;
    uint32_t rodata_start = (uint32_t)&_rodata_start - (uint32_t)&_kernel_offset;
    uint32_t rodata_end = (uint32_t)&_rodata_end - (uint32_t)&_kernel_offset;
    for (int i = 0; i < 1024; ++i) {
        uint16_t flags;
        if (paddr >= text_start && paddr < text_end) {
            flags = 0x1;    // present, read-only
        }
        else if (paddr >= rodata_start && paddr < rodata_end) {
            flags = 0x1;    // present, read-only
        }
        else {
            flags = 0x3;    // present, read-write
        }
        offset_table[i] = paddr | flags;
        paddr += 0x1000;
    }
    uint32_t kernel_vma = (uint32_t)&_kernel_vma;
    dir[kernel_vma >> 22] = (uint32_t *)((uint32_t)offset_table | 0x3);

    // Map last entry of page directory to itself
    dir[1023] = (uint32_t *)((uint32_t)dir | 0x3);

    // Load PDE into CR3
    asm volatile("movl %0, %%cr3" : : "r" (dir));

    // Enable paging and protection
    uint32_t cr0;
    asm volatile("movl %%cr0, %0" : "=r" (cr0));
    cr0 |= 0x80000001;
    asm volatile("movl %0, %%cr0" : : "r" (cr0));
}

}
