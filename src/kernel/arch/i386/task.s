.global switch_task
.type switch_task, @function
switch_task:
    cli

    push %ebx
    push %esi
    push %edi
    push %ebp

    mov 24(%esp), %edx
    mov (%edx), %edi
    mov %esp, (%edi)

    mov 20(%esp), %esi
    mov %esi, (%edx)

    mov (%esi), %esp
    mov 4(%esi), %ebx
    mov 8(%esi), %eax
    # mov %ebx,         update TSS ESP0
    mov %cr3, %ecx

    cmp %eax, %ecx
    je 1f
    mov %eax, %cr3
1:

    pop %ebp
    pop %edi
    pop %esi
    pop %ebx

    sti
    ret


.global jump_to_usermode
.type jump_to_usermode, @function
jump_to_usermode:
    cli

    mov $0x23, %ax      # get user data segment with DPL 3
    mov %ax, %ds
    mov %ax, %es
    mov %ax, %fs
    mov %ax, %gs

    mov %esp, %eax
    pushl $0x23         # user data segment
    pushl %eax          # stack pointer
    pushf               # EFLAGS
    popl %eax
    orl $0x200, %eax    # set IF
    pushl %eax
    pushl $0x1B         # user code segment
    pushl $1f
    iret
1:


.global enter_region
.type enter_region, @function
enter_region:
    push %ebp
    mov %esp, %ebp

    mov 8(%ebp), %edx
    mov $1, %eax
    xchg %eax, (%edx)
    cmpb $0, %eax
    jne enter_region

    pop %ebp
    ret


.global leave_region
.type leave_region, @function
leave_region:
    push %ebp
    mov %esp, %ebp

    mov 8(%ebp), %edx
    movl $0, (%edx)

    pop %ebp
    ret
