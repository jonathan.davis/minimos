#include <kernel/kernel.h>
#include <machine/machine.h>
#include <machine/pit.h>

#include <stdio.h>


void Machine::init_segmentation()
{
    // Flat memory segmentation setup
    // Flags:
    //   0xC - 4 KiB granularity, 32-bit segment
    // Access:
    //   0x9A - present, ring 0, code/data, execute/read
    //   0x92 - present, ring 0, code/data, read/write
    //   0xFA - present, ring 3, code/data, execute/read
    //   0xF2 - present, ring 3, code/data, read/write
    //   0xE9 - present, ring 3, not busy (TSS entry)
    m_gdt.set_entry(0, GlobalDescriptorTable::Entry(0x0, 0x0, 0x0, 0x0));           // null descriptor
    m_gdt.set_entry(1, GlobalDescriptorTable::Entry(0x0, 0xFFFFFFFF, 0x9A, 0xC));   // code
    m_gdt.set_entry(2, GlobalDescriptorTable::Entry(0x0, 0xFFFFFFFF, 0x92, 0xC));   // data
    m_gdt.set_entry(3, GlobalDescriptorTable::Entry(0x0, 0xFFFFFFFF, 0xFA, 0xC));   // user code
    m_gdt.set_entry(4, GlobalDescriptorTable::Entry(0x0, 0xFFFFFFFF, 0xF2, 0xC));   // user data

    uint32_t tss_base = reinterpret_cast<uint32_t>(&m_tss);
    uint32_t tss_limit = sizeof(TaskStateSegment);
    m_gdt.set_entry(5, GlobalDescriptorTable::Entry(tss_base, tss_limit, 0xE9, 0x0)); // TSS

    m_tss.m_ss0 = 0x10; // Kernel data segment, GDT, ring 0
    m_tss.m_esp0 = 0x0;
    m_tss.m_cs = 0x0B;
    m_tss.m_ss = 0x13;
    m_tss.m_ds = 0x13;
    m_tss.m_es = 0x13;
    m_tss.m_fs = 0x13;
    m_tss.m_gs = 0x13;

    m_gdt.load();
    printf("[  OK  ] Initialized GDT\n");
}


void Machine::init_interrupts()
{
    m_pic.init();

    // 0x08 selector => Kernel code segment, GDT, ring 0
    // 0x8E type => present, 32-bit interrupt
    m_idt.set_entry(static_cast<uint8_t>(Interrupt::divide_by_zero), InterruptDescriptorTable::Entry((uint32_t)divide_by_zero_handler, 0x08, 0x8E));
    m_idt.set_entry(static_cast<uint8_t>(Interrupt::debug), InterruptDescriptorTable::Entry((uint32_t)debug_handler, 0x08, 0x8E));
    m_idt.set_entry(static_cast<uint8_t>(Interrupt::nonmaskable_interrupt), InterruptDescriptorTable::Entry((uint32_t)nonmaskable_interrupt_handler, 0x08, 0x8E));
    m_idt.set_entry(static_cast<uint8_t>(Interrupt::breakpoint), InterruptDescriptorTable::Entry((uint32_t)breakpoint_handler, 0x08, 0x8E));
    m_idt.set_entry(static_cast<uint8_t>(Interrupt::overflow), InterruptDescriptorTable::Entry((uint32_t)overflow_handler, 0x08, 0x8E));
    m_idt.set_entry(static_cast<uint8_t>(Interrupt::bound_range_exceeded), InterruptDescriptorTable::Entry((uint32_t)bound_range_exceeded_handler, 0x08, 0x8E));
    m_idt.set_entry(static_cast<uint8_t>(Interrupt::invalid_opcode), InterruptDescriptorTable::Entry((uint32_t)invalid_opcode_handler, 0x08, 0x8E));
    m_idt.set_entry(static_cast<uint8_t>(Interrupt::device_not_available), InterruptDescriptorTable::Entry((uint32_t)device_not_available_handler, 0x08, 0x8E));
    m_idt.set_entry(static_cast<uint8_t>(Interrupt::double_fault), InterruptDescriptorTable::Entry((uint32_t)double_fault_handler, 0x08, 0x8E));
    m_idt.set_entry(static_cast<uint8_t>(Interrupt::coprocessor_segment_overrun), InterruptDescriptorTable::Entry((uint32_t)coprocessor_segment_overrun_handler, 0x08, 0x8E));
    m_idt.set_entry(static_cast<uint8_t>(Interrupt::invalid_tss), InterruptDescriptorTable::Entry((uint32_t)invalid_tss_handler, 0x08, 0x8E));
    m_idt.set_entry(static_cast<uint8_t>(Interrupt::segment_not_present), InterruptDescriptorTable::Entry((uint32_t)segment_not_present_handler, 0x08, 0x8E));
    m_idt.set_entry(static_cast<uint8_t>(Interrupt::stack_segment_fault), InterruptDescriptorTable::Entry((uint32_t)stack_segment_fault_handler, 0x08, 0x8E));
    m_idt.set_entry(static_cast<uint8_t>(Interrupt::general_protection_fault), InterruptDescriptorTable::Entry((uint32_t)general_protection_handler, 0x08, 0x8E));
    m_idt.set_entry(static_cast<uint8_t>(Interrupt::page_fault), InterruptDescriptorTable::Entry((uint32_t)page_fault_handler, 0x08, 0x8E));
    // Intel-reserved interrupt 15
    m_idt.set_entry(static_cast<uint8_t>(Interrupt::math_fault), InterruptDescriptorTable::Entry((uint32_t)math_fault_handler, 0x08, 0x8E));
    m_idt.set_entry(static_cast<uint8_t>(Interrupt::alignment_check), InterruptDescriptorTable::Entry((uint32_t)alignment_check_handler, 0x08, 0x8E));
    m_idt.set_entry(static_cast<uint8_t>(Interrupt::machine_check), InterruptDescriptorTable::Entry((uint32_t)machine_check_handler, 0x08, 0x8E));
    m_idt.set_entry(static_cast<uint8_t>(Interrupt::simd_fpe), InterruptDescriptorTable::Entry((uint32_t)simd_fpe_handler, 0x08, 0x8E));
    m_idt.set_entry(static_cast<uint8_t>(Interrupt::virtualization), InterruptDescriptorTable::Entry((uint32_t)virtualization_handler, 0x08, 0x8E));
    // Intel-reserved interrupts 21-31

    // Interrupt requests
    m_idt.set_entry(static_cast<uint8_t>(Interrupt::timer), InterruptDescriptorTable::Entry((uint32_t)timer_interrupt, 0x08, 0x8E));
    m_idt.set_entry(static_cast<uint8_t>(Interrupt::keyboard), InterruptDescriptorTable::Entry((uint32_t)keyboard_interrupt, 0x08, 0x8E));

    m_idt.load();
    printf("[  OK  ] Initialized IDT\n");
}


void Machine::init_timer()
{
    m_timer = new PIT();
    m_timer->set_frequency(1000);
    printf("[  OK  ] Set timer to 1 kHz\n");
}


void Machine::init()
{
    init_segmentation();
    init_interrupts();
    init_timer();
}
