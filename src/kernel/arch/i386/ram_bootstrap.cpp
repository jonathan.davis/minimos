#include <machine/paging.h>
#include <machine/ram_bootstrap.h>

extern uint32_t _kernel_lma;
extern uint32_t _kernel_vma;
extern uint32_t _kernel_offset;
extern uint32_t _kernel_end;
extern uint32_t _text_start;
extern uint32_t _text_end;
extern uint32_t _rodata_start;
extern uint32_t _rodata_end;


uint32_t kmalloc(uint32_t *placement_addr, uint32_t size)
{
    uint32_t addr = *placement_addr;
    *placement_addr += size;
    return addr;
}

uint32_t kmalloc_a(uint32_t *placement_addr, uint32_t size)
{
    *placement_addr = (*placement_addr + 0xFFF) & 0xFFFFF000;
    return kmalloc(placement_addr, size);
}

uint32_t kmalloc_p(uint32_t *placement_addr, unsigned int npages)
{
    return kmalloc_a(placement_addr, npages * PAGE_SIZE);
}


ram_bootstrap_t *bootstrap_ram(multiboot_info_t *mbd, uint32_t boot_esp)
{
    // Initialize kmalloc
    uint32_t placement_addr = (uint32_t)(&_kernel_end) - (uint32_t)(&_kernel_offset);

    // Allocate the bootstrap table
    ram_bootstrap_t *ram_bootstrap = (ram_bootstrap_t *)kmalloc(&placement_addr, sizeof(ram_bootstrap_t));

    // Create initial thread stack
    uint32_t stack_pages = INITIAL_STACK_SIZE / PAGE_SIZE;
    uint32_t stack_ptr = kmalloc_p(&placement_addr, stack_pages);
    ram_bootstrap->m_stack_ptr = stack_ptr + stack_pages * PAGE_SIZE;    // Set to top of stack

    // Allocate page directory and tables
    ram_bootstrap->m_page_dir = kmalloc_p(&placement_addr, 1);
    uint32_t identity_table = kmalloc_p(&placement_addr, 1);
    uint32_t offset_table = kmalloc_p(&placement_addr, 1);

    // Allocate physical page stack
    ram_bootstrap->m_physical_stack_lower_capacity = mbd->mem_lower / 4;
    ram_bootstrap->m_physical_stack_lower = kmalloc(&placement_addr, ram_bootstrap->m_physical_stack_lower_capacity * sizeof(uint32_t));
    ram_bootstrap->m_physical_stack_upper_capacity = mbd->mem_upper / 4;
    ram_bootstrap->m_physical_stack_upper = kmalloc(&placement_addr, ram_bootstrap->m_physical_stack_upper_capacity * sizeof(uint32_t));

    // Allocate virtual address list
    ram_bootstrap->m_virtual_list = kmalloc_p(&placement_addr, 1);
    ram_bootstrap->m_virtual_list_limit = 0x1000;

    // Save Multiboot info structure
    ram_bootstrap->m_multiboot_info = mbd;

    enable_paging((uint32_t **)ram_bootstrap->m_page_dir, (uint32_t *)identity_table, (uint32_t *)offset_table);

    // Move RAM bootstrap info to higher half
    ram_bootstrap = (ram_bootstrap_t *)((uint32_t)ram_bootstrap + (uint32_t)&_kernel_offset);
    return ram_bootstrap;
}
