.section .init
    /* crtend.o .init will end up here */
    popl %ebp
    ret

.section .fini
    /* crtend.o .fini will end up here */
    popl %ebp
    ret
