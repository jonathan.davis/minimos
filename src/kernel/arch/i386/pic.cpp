#include <machine/pic.h>
#include <machine/common.h>
#include <machine/idt.h>
#include <stdint.h>

#define ICW1_ICW4       0x01
#define ICW1_SINGLE     0x02
#define ICW1_INTERVAL4  0x04
#define ICW1_LEVEL      0x08
#define ICW1_INIT       0x10

#define ICW4_8086       0x01
#define ICW4_AUTO       0x02
#define ICW4_BUF_SLAVE  0x08
#define ICW4_BUF_MASTER 0x0C
#define ICW4_SFNM       0x10


uint8_t PIC::m_offset1 = 0;
uint8_t PIC::m_offset2 = 0;
uint8_t PIC::m_mask1 = 0;
uint8_t PIC::m_mask2 = 0;


void PIC::init()
{
    remap(PIC_OFFSET1, PIC_OFFSET2);

    // Unmask all interrupts
    m_mask1 = 0x00;
    m_mask2 = 0x00;
    enable();
}


void PIC::enable()
{
    outb(PIC1_DATA, m_mask1);
    outb(PIC2_DATA, m_mask2);
}


void PIC::disable()
{
    m_mask1 = inb(PIC1_DATA);
    m_mask2 = inb(PIC2_DATA);
    outb(PIC1_DATA, 0xFF);
    outb(PIC2_DATA, 0xFF);
}


void PIC::enable_interrupt(uint8_t irq)
{
    uint16_t port;
    uint8_t mask;

    if (irq < 8) {
        port = PIC1_DATA;
    } else {
        port = PIC2_DATA;
        irq -= 8;
    }

    mask = inb(port) & ~(1 << irq);
    outb(port, mask);
}


void PIC::disable_interrupt(uint8_t irq)
{
    uint16_t port;
    uint8_t mask;

    if (irq < 8) {
        port = PIC1_DATA;
    } else {
        port = PIC2_DATA;
        irq -= 8;
    }

    mask = inb(port) | (1 << irq);
    outb(port, mask);
}


void PIC::send_eoi(uint8_t irq)
{
    irq -= m_offset1;

    if (irq >= 8) {
        outb(PIC2_COMMAND, PIC_EOI);
    }

    outb(PIC1_COMMAND, PIC_EOI);
}


void PIC::remap(uint32_t offset1, uint32_t offset2)
{
    m_offset1 = offset1;
    m_offset2 = offset2;

    uint8_t mask1, mask2;

    mask1 = inb(PIC1_DATA);
    mask2 = inb(PIC2_DATA);
    
    outb(PIC1_COMMAND, ICW1_INIT | ICW1_ICW4);
    outb(PIC2_COMMAND, ICW1_INIT | ICW1_ICW4);
    outb(PIC1_DATA, offset1);
    outb(PIC2_DATA, offset2);
    outb(PIC1_DATA, 4);
    outb(PIC2_DATA, 2);

    outb(PIC1_DATA, ICW4_8086);
    outb(PIC2_DATA, ICW4_8086);

    outb(PIC1_DATA, mask1);
    outb(PIC2_DATA, mask2);
}
