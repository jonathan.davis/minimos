#ifndef _PAGING_H
#define _PAGING_H

#include <stdint.h>

/**
 * Virtual address of the page directory.
 * Defined due to the last page directory entry being mapped to the page directory itself.
 */
#define PAGE_DIRECTORY_VIRTUAL_ADDRESS  0xFFFFF000

/**
 * Virtual address of the first page table.
 * Defined due to the last page directory entry being mapped to the page directory itself.
 */
#define PAGE_TABLE0_VIRTUAL_ADDRESS     0xFFC00000

/** 4 KiB pages in x86. */
#define PAGE_SIZE                       0x1000
#define PAGE_TABLE_SIZE                 1024
#define PAGE_DIRECTORY_SIZE             1024


struct Page
{
    uint32_t m_flags:12;
    uint32_t m_address:20;
}__attribute__((packed));


struct PageTable
{
    Page m_entries[PAGE_TABLE_SIZE];
};


struct PageDirectory
{
    PageTable *m_entries[PAGE_DIRECTORY_SIZE];
};


extern "C" {

void enable_paging(uint32_t **dir, uint32_t *identity_table, uint32_t *offset_table);

}

#endif
