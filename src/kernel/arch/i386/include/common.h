#ifndef _COMMON_H
#define _COMMON_H

#include <stdint.h>


/**
 * Outputs a byte to a given port.
 * @param port output port
 * @param data data to send
 */
static inline void outb(uint16_t port, uint8_t data)
{
    asm volatile ("outb %1, %0" : : "dN" (port), "a" (data));
}


/**
 * Gets an input byte from a given port.
 * @param port input port
 * @return byte from input port
 */
static inline uint8_t inb(uint16_t port)
{
    uint8_t ret;
    asm volatile ("inb %1, %0" : "=a" (ret) : "dN" (port));
    return ret;
}

#endif
