#ifndef _PIT_H
#define _PIT_H

#include <machine/timer.h>


/**
 * Abstraction of the Programmable Interval Timer.
 * Provides interfacing for setting the PIT frequency.
 */
class PIT : public Timer
{
public:
    /**
     * Sets the PIT frequency.
     * @param frequency between 18 Hz and 1193181 Hz
     */
    void set_frequency(uint32_t frequency);
};

#endif
