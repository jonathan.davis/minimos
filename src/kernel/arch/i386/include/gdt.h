#ifndef _GDT_H
#define _GDT_H

#include <stdint.h>

#define GDT_SIZE 6


/**
 * Global Descriptor Table.
 * Abstracts the i386 GDT.
 * Provides operations for creating segment entries and loading them into the processor.
 */
class GlobalDescriptorTable
{
public:
    /**
     * GDT Entry.
     * Structure for physical layout of GDT entry in memory.
     */
    struct Entry {
        uint16_t m_limit0_15;
        uint16_t m_base0_15;
        uint8_t m_base16_23;
        uint8_t m_access;
        uint8_t m_limit16_19:4;
        uint8_t m_flags:4;
        uint8_t m_base24_31;

        /**
         * Default constructor.
         * Creates an uninitialized GDT entry.
         */
        Entry() = default;

        /**
         * Entry constructor.
         * Initializes a GDT entry packed according to the Intel specification.
         * @param base segment base address
         * @param limit segment limit address
         * @param access segment access bits
         * @param flags segment flag bits
         */
        Entry(uint32_t base, uint32_t limit, uint8_t access, uint8_t flags);
    }__attribute__((packed));
private:
    /**
     * Pointer to the global descriptor table.
     * Packs the GDT pointer according to the Intel specification.
     */
    struct GDTPtr {
        uint16_t m_limit;
        Entry *m_base;
    }__attribute__((packed));

    GDTPtr m_ptr;               /**< Provided to the processor to load the GDT. */
    Entry m_entries[GDT_SIZE];  /**< The actual descriptor table in memory. */
public:
    /**
     * Constructs the Global Descriptor Table.
     * Initializes m_ptr to contain the pointer to m_entries as well as the address limit.
     * @see m_ptr
     * @see m_entries
     */
    GlobalDescriptorTable();

    /**
     * Places a GDT Entry into the table.
     * @see Entry
     * @param index index of the entry in the GDT
     * @param entry entry structure to store
     */
    void set_entry(unsigned int index, Entry entry);

    /**
     * Loads the GDT into the processor.
     * @note Disables interrupts and does not restore them
     * @see gdt_flush
     */
    void load();
};


/**
 * Flushes the GDT.
 * External assembly function.
 * Executes the lgdt instruction and flushes the segment registers.
 * @note Disables interrupts and does not restore them.
 * @param ptr pointer to the GDT
 */
extern void gdt_flush(uint32_t ptr);

extern void tss_flush();


#endif
