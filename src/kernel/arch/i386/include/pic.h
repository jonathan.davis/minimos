#ifndef _PIC_H
#define _PIC_H

#include <stdint.h>

#define PIC1            0x20        // IO base addr for master PIC.
#define PIC2            0xA0        // IO base addr for slave PIC
#define PIC1_COMMAND    PIC1
#define PIC2_COMMAND    PIC2
#define PIC1_DATA       (PIC1+1)
#define PIC2_DATA       (PIC2+1)

#define PIC_EOI         0x20

#define PIC_OFFSET1     0x20
#define PIC_OFFSET2     0x28

#define IA32_APIC_BASE_MSR          0x1B
#define IA32_APIC_BASE_MSR_BSP      0x100
#define IA32_APIC_BASE_MSR_ENABLE   0x800


/**
 * Abstraction of the Programmable Interrupt Controller.
 * Provides operations for remapping the PIC, enabling, and disable interrupts.
 */
class PIC
{
private:
    static uint8_t m_offset1;   /**< Offset of IRQ 0. */
    static uint8_t m_offset2;   /**< Offset of IRQ 8. */

    static uint8_t m_mask1;     /**< Saved mask for PIC1. */
    static uint8_t m_mask2;     /**< Saved mask for PIC2. */

    /**
     * Remaps the PIC from interrupt 0 to a new location.
     * This is necessary due to the overlapping between Intel exceptions and the default hardware interrupts.
     * @param offset1 offset of IRQ 0
     * @param offset2 offset of IRQ 8
     */
    void remap(uint32_t offset1, uint32_t offset2);
public:
    PIC() = default;

    /**
     * Initializes the PIC.
     * Remaps the IRQ offsets and enables all interrupts.
     * @see remap()
     * @see enable_interrupt()
     */
    void init();

    /**
     * Enables the PIC.
     * Restores the cached masks from disable().
     * @see disable()
     */
    void enable();

    /**
     * Disables the PIC.
     * Saves the interrupt masks and disables all interrupts.
     * @see enable()
     */
    void disable();

    /**
     * Enables a single interrupt.
     * @param irq interrupt index to enable
     */
    void enable_interrupt(uint8_t irq);

    /**
     * Disables a single interrupt.
     * @param irq interrupt index to disable.
     */
    void disable_interrupt(uint8_t irq);

    /**
     * Sends the end-of-interrupt signal.
     * @note Static due to needing global access from interrupt service routines.
     * @param irq interrupt index to send EOI signal
     */
    static void send_eoi(uint8_t irq);
};


class APIC
{
private:
    uint32_t *m_base;
public:
    APIC();

    uint32_t read_register(uint16_t reg);
    void write_register(uint16_t reg, uint32_t val);

    void set_base(uint32_t *ptr);
    uint32_t *get_base();
    void enable();
};


#endif
