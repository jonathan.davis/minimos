#ifndef _MACHINE_H
#define _MACHINE_H

#include <machine/gdt.h>
#include <machine/idt.h>
#include <machine/pic.h>
#include <machine/timer.h>
#include <machine/task.h>


class Machine
{
private:
    GlobalDescriptorTable m_gdt;
    InterruptDescriptorTable m_idt;
    PIC m_pic;
    Timer *m_timer;
    TaskStateSegment m_tss;

    /**
     * Initializes the Global Descriptor Table.
     * Defines a flat segment memory model and loads the GDT.
     */
    void init_segmentation();

    /**
     * Initializes interrupts.
     * Initializes the programmable interrupt controller, loads the interrupt descriptor table, and registers interrupt
     * service routines.
     */
    void init_interrupts();

    void init_timer();
public:
    Machine() = default;

    void init();
};

#endif
