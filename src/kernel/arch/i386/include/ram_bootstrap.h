#ifndef _RAM_BOOTSTRAP_H
#define _RAM_BOOTSTRAP_H

#include <kernel/multiboot.h>

#define INITIAL_STACK_SIZE  0x4000  // 16 KiB

extern "C" {

typedef struct {
    uint32_t m_stack_ptr;
    uint32_t m_page_dir;
    uint32_t m_physical_stack_lower;
    uint32_t m_physical_stack_upper;
    uint32_t m_physical_stack_lower_capacity;
    uint32_t m_physical_stack_upper_capacity;
    uint32_t m_virtual_list;
    uint32_t m_virtual_list_limit;
    multiboot_info_t *m_multiboot_info;
} ram_bootstrap_t;


ram_bootstrap_t *bootstrap_ram(multiboot_info_t *mbd, uint32_t boot_esp);

}

#endif
