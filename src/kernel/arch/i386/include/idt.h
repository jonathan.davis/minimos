#ifndef _IDT_H
#define _IDT_H

#include <stdint.h>

#define IDT_SIZE 256


/**
 * Interrupt Descriptor Table.
 * Abstracts the i386 IDT.
 * Provides operations for creating interrupt entries and loading them into the processor.
 */
class InterruptDescriptorTable
{
public:
    /**
     * IDT Entry.
     * Structure for physical layout of IDT entry in memory.
     */
    struct Entry {
        uint16_t m_base0_15;
        uint16_t m_selector;
        uint8_t m_zero = 0;
        uint8_t m_type;
        uint16_t m_base16_31;

        /**
         * Default constructor.
         * Creates an uninitialized IDT entry.
         */
        Entry() = default;

        /**
         * Entry constructor.
         * Initializes an IDT entry packed according to the Intel specification.
         * @param base interrupt service routine address
         * @param selector segment selector
         * @param type interrupt type
         */
        Entry(uint32_t base, uint16_t selector, uint8_t type);
    }__attribute__((packed));
private:
    /**
     * Pointer to the interrupt descriptor table.
     * Packs the IDT pointer according to the Intel specification.
     */
    struct IDTPtr {
        uint16_t m_limit;
        Entry *m_base;
    }__attribute__((packed));

    IDTPtr m_ptr;               /**< Provided to the processor to load the IDT. */
    Entry m_entries[IDT_SIZE];  /**< The actual descriptor table in memory. */
public:
    /**
     * Constructs the Interrupt Descriptor Table.
     * Initializes m_ptr to contain the pointer to m_entries as well as the address limit.
     * @see m_ptr
     * @see m_entries
     */
    InterruptDescriptorTable();

    /**
     * Places an IDT Entry into the table.
     * @see Entry
     * @param index index of the entry in the IDT
     * @param entry entry structure to store
     */
    void set_entry(unsigned int index, Entry entry);

    /**
     * Loads the IDT into the processor.
     * Loads the IDT using the lidt instruction and enables interrupts.
     * @note Enables interrupts
     */
    void load();
};

#endif

