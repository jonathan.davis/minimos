#ifndef _KERNEL_TTY_H
#define _KERNEL_TTY_H

#include <stddef.h>

/**
 * Initialize the terminal
 * Sets static variables and clears the screen
 */
void terminal_initialize();


/**
 * Places the character at the cursor.
 * Takes a character c and outputs it at the current position of the cursor.
 * @param c character to output
 */
void terminal_putchar(char c);


/**
 * Writes the given data to the terminal.
 * Writes the given data to the terminal, starting at the current cursor position.
 * @param data pointer to the data to write
 * @param size size of the data to write
 */
void terminal_write(const char *data, size_t size);


/**
 * Writes the string to the terminal.
 * Writes the given null-terminated string to the terminal, starting at the current cursor position.
 * @param str null-terminated string to write
 */
void terminal_writestring(const char *str);


#endif
