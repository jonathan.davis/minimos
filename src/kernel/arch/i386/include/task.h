#ifndef _TASK_H
#define _TASK_H

#include <stdint.h>

struct TaskStateSegment
{
    uint32_t m_previous_task_link;
    uint32_t m_esp0;
    uint32_t m_ss0;
    uint32_t m_esp1;
    uint32_t m_ss1;
    uint32_t m_esp2;
    uint32_t m_ss2;
    uint32_t m_cr3;
    uint32_t m_eip;
    uint32_t m_eflags;
    uint32_t m_eax;
    uint32_t m_ecx;
    uint32_t m_edx;
    uint32_t m_ebx;
    uint32_t m_esp;
    uint32_t m_ebp;
    uint32_t m_esi;
    uint32_t m_edi;
    uint32_t m_es;
    uint32_t m_cs;
    uint32_t m_ss;
    uint32_t m_ds;
    uint32_t m_fs;
    uint32_t m_gs;
    uint32_t m_ldt_segment_selector;
    uint16_t m_debug_trap_flag;
    uint16_t m_io_map_base;
}__attribute((packed));


struct Task
{
    enum class State {
        running,
        ready,
        blocked,
        suspended,
        terminated
    };

    uint32_t m_esp;
    uint32_t m_ebp;
    uint32_t m_page_directory;
    State m_state;
    Task *m_next;
    uint16_t m_id;
};


extern void switch_task(Task *next_task, Task **current_task);

extern void jump_to_usermode();

extern void enter_region(bool *lock);

extern void leave_region(bool *lock);

#endif