# Multiboot header constants.
.set ALIGN,         1 << 0                  # align loaded modules on page boundaries
.set MEMINFO,       1 << 1                  # provide memory map
.set FLAGS,         ALIGN | MEMINFO         # multiboot flag field
.set KERNEL_MAGIC,  0x1BADB002              # kernel magic number
.set BOOT_MAGIC,    0x2BADB002              # bootloader magic number
.set CHECKSUM,      -(KERNEL_MAGIC + FLAGS) # multiboot checksum

.set _kernel_lma, 0x00100000
.set _kernel_vma, 0xC0000000
.set _kernel_offset, _kernel_vma - _kernel_lma

# Create multiboot header.
.section .multiboot
.align 4
.long KERNEL_MAGIC
.long FLAGS
.long CHECKSUM

# Kernel entry point.
.section .text
.global _start
.type _start, @function
_start:
    # check to make sure that bootloader is multiboot compliant
    cmpl $BOOT_MAGIC, %eax
    jne halt

    # pass multiboot header and inital stack pointer to bootstrap_ram()
    pushl %esp
    pushl %ebx
    call bootstrap_ram
    addl $8, %esp

    # get new stack pointer from ram_info
    movl (%eax), %esp

    # jump to higher-half
    lea 1f, %ecx
    jmp *%ecx
1:
    # move stack ptr to higher half
    addl $_kernel_offset, %esp

    # push bootstrapped RAM info onto stack for kernel_main
    pushl %eax

    # C++ runtime init
    call _init

    call kernel_main
    addl $4, %esp

    # finalize global C++ objects
    subl $4, %esp
    movl $0x0, (%esp)
    call __cxa_finalize
    addl $4, %esp

    # hang if we are to return from kernel main.
halt:
    cli
1:  hlt
    jmp 1b
.size _start, . - _start
