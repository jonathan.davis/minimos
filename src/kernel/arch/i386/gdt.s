# Initialize GDT
.section .text
.global gdt_flush
.type gdt_flush, @function
gdt_flush:
    cli                     # disable interrupts 
    movl 4(%esp), %eax      # get gdt ptr from the stack
    lgdtl (%eax)            # inform processor of gdt
    mov %cr0, %eax
    orl $0x00000001, %eax   # set protected mode enable
    mov %eax, %cr0
    mov $0x10, %ax          # reload segment registers
    mov %ax, %ds
    mov %ax, %es
    mov %ax, %fs
    mov %ax, %gs
    mov %ax, %ss
    ljmp $0x08, $next
next:
    ret

# Flush TSS
.section .text
.global tss_flush
.type tss_flush, @function
tss_flush:
    mov $0x2B, %ax          # get TSS selector segment with DPL 3
    ltr %ax                 # load into the task state register
    ret
