#include <limits.h>
#include <stdbool.h>
#include <stdarg.h>
#include <stdio.h>
#include <string.h>


/* Prints data to stdout.
 * Returns true if all of the data was printed.
 * Returns false if EOF was reached.
 */
static bool print(const char *data, size_t length)
{
    const unsigned char *bytes = (const unsigned char *)data;
    for (size_t i = 0; i < length; i++) {
        if (putchar(bytes[i]) == EOF) {
            return false;
        }
    }
    return true;
}


int printf(const char* restrict format, ...)
{
    va_list parameters;
    va_start(parameters, format);

    int written = 0;        // The number of bytes written to stdout.

    while (*format != '\0') {
        size_t maxrem = INT_MAX - written;

        if (format[0] != '%' || format[1] == '%') {
            if (format[0] == '%') {
                format++;
            }
            size_t amount = 1;
            while (format[amount] && format[amount] != '%') {
                amount++;
            }
            if (maxrem < amount) {
                // TODO: set errno to EOVERFLOW
                return -1;
            }
            if (!print(format, amount)) {
                return -1;
            }
            format += amount;
            written += amount;
            continue;
        }

        const char *format_begun_at = format++;

        if (*format == 'c') {
            format++;
            char c = (char) va_arg(parameters, int);
            if (!maxrem) {
                // TODO: set errno to EOVERFLOW
                return -1;
            }
            if (!print(&c, sizeof(c))) {
                return -1;
            }
            written++;
        } else if (*format == 's') {
            format++;
            const char *str = va_arg(parameters, const char*);
            size_t len = strlen(str);
            if (maxrem < len) {
                // TODO: set errno to EOVERFLOW
                return -1;
            }
            if (!print(str, len)) {
                return -1;
            }
            written += len;
        } else if (*format == 'x') {
            format++;
            unsigned int value = va_arg(parameters, unsigned int);
            size_t len = sizeof(unsigned int) * 2 + 2;      // Room for hex and 0x prefix.
            char str[len+1];
            str[0] = '0';
            str[1] = 'x';
            str[len] = '\0';    // NUL terminator.
            if (maxrem < len) {
                // TODO: set errno to EOVERFLOW
                return -1;
            }
            for (size_t i = len-1; i >= 2; i--) {
                str[i] = value % 0x10;
                if (str[i] < 0xA)       // Print characters 0-9.
                    str[i] += 0x30;
                else                    // Print characters a-f.
                    str[i] += 0x57;
                value /= 0x10;
            }
            if (!print(str, len)) {
                return -1;
            }
            written += len;
        } else {
            format = format_begun_at;
            size_t len = strlen(format);
            if (maxrem < len) {
                // TODO: set errno to EOVERFLOW
                return -1;
            }
            if (!print(format, len))
                return -1;
            written += len;
            format += len;
        }
    }

    va_end(parameters);
    return written;
}
