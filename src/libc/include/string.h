#ifndef _STRING_H
#define _STRING_H

#include <sys/cdefs.h>

#include <stddef.h>

#ifdef __cplusplus
extern "C" {
#endif

/* Compares two strings.
 * Returns -1 if the first string is less than the second.
 * Returns 1 if the first string is greater than the second.
 * Returns 0 if the two strings are equal.
 */
int memcmp(const void*, const void*, size_t);
/* Copies memory from the src to the dst */
void* memcpy(void* __restrict, const void* __restrict, size_t);
/* Moves memory from the src to the dst */
void* memmove(void*, const void*, size_t);
/* Sets the memory at the given location to the value */
void* memset(void*, int, size_t);
/* Finds the length of the string */
size_t strlen(const char*);

#ifdef __cplusplus
}
#endif

#endif
