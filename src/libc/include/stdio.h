#ifndef _STDIO_H
#define _STDIO_H

#include <sys/cdefs.h>

#define EOF (-1)

#ifdef __cplusplus
extern "C" {
#endif

/* Prints a formatted string to stdout */
int printf(const char* __restrict, ...);
/* Prints a single character to stdout */
int putchar(int);
/* Prints a string to stdout */
int puts(const char*);

#ifdef __cplusplus
}
#endif

#endif
