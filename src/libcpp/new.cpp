#include <kernel/kernel.h>


void *operator new(unsigned long size)
{
    return Kernel::heap().alloc(size);
}

void *operator new[](unsigned long size)
{
    return Kernel::heap().alloc(size);
}

void operator delete(void *p)
{
    Kernel::heap().free(p);
}

void operator delete[](void *p)
{
    Kernel::heap().free(p);
}
